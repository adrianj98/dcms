(function($){
    RegExp.escape= function(s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    };
    dcms = {


        itemFactories : {},
        getValues :function(data){
            _.each(APP.schema.fields, function (field) {
                var value = dcms.getValue(field);

                if (value !== undefined)
                    data[field.ns] = value;
                var editFactory = dcms.getEditFactory(field.type);
                if (editFactory)
                if (editFactory.enabled(field) === false)
                   data[field.ns] = null;

            });
            data._inherited = undefined;
            return data;
        },
        getValue : function(field){
            var editFactory = dcms.getEditFactory(field.type);

            return editFactory?editFactory.value(field):undefined;
        },
        defaultValue : function(field){
            var value = $('[property="' + field.ns + '"]').val();
            return value;
        },
        enabledControl : function(field, enable){
            var el = $('[property="' + field.ns + '"]');
            if ( enable === undefined){
                   if (el.length == 0 )
                      return undefined;
                   return !el.is(':disabled');
            }else
            {

            if (enable)
                el.removeAttr('disabled');
            else
                el.attr('disabled','disabled');
            }
        },
        getDisplay : function(data,field){
        var value = data[field.ns]?data[field.ns]:'';
            return value;
        },
        registerEditFactory : function(itemType,factory){
            if (_.isFunction(factory))
                factory = {create :factory};
            _.defaults(factory,{value : dcms.defaultValue, enabled : dcms.enabledControl});

            dcms.itemFactories[itemType] = factory;
        },

        getEditFactory : function(itemType){
            return dcms.itemFactories[itemType];
        },
        createControl : function(item,options){
            if (!item.editable) {
                return undefined;
            }
            var editFactory = dcms.getEditFactory(item.type);

            var edit = editFactory.create(item);
            if (options.disable){
            var patt = new RegExp('^'+RegExp.escape('root.variants.'+variant_id+'.')+'');
            var isSet = patt.test(item.ns)  ;
            if (!isSet)
                edit.attr('disabled','disabled');
            }


            var li = $('<'+options.item_tag+'><label>'+item.caption+':</label></'+options.item_tag+'>');
            if(options.item_class)
                li.addClass(options.item_class);

            if(options.showNamespace)
                li.append('<span>'+item.ns+'</span>');
            var inh = $('<a class="small"></a>');
            if (item.value == undefined){

                inh.attr('href','#disable');
            } else
            {
                inh.attr('href','#enable');

            }
            li.append(inh);

                setTimeout(function(){
                    if (item.group){
                    var selector = $('[property="'+item.group_field+'"]');
                    var changed = function(){
                        var result = dcms.getValue(APP.schema.fields[item.group_field]);
                        if (result == item.group){
                            li.show();
                        }
                        else
                        {
                            li.hide();
                        }
                    };
                    selector.change(changed);
                    changed();
                    }
                    toggleInherited();
                },1);



            li.append(edit);
            li.find('input,select').addClass('form-control');
            var toggleInherited = function(e){
                if (e !== undefined)
                   e.preventDefault();
                var ac = $(inh).attr('href');
                if (ac == '#enable'){

                    editFactory.enabled(item,true);
                    $(inh).attr('href','#disable');
                    $(inh).html('inherit');

                }else
                {
                    editFactory.enabled(item,false);

                    $(inh).attr('href','#enable');
                    $(inh).html(' Override Inherited');
                }
                $(inh).unbind().click(toggleInherited);
            };



            if (item.help)
                li.append('<p class="help-block">'+item.help+'<p>');

            return li;
        },

        addPanelItems : function(dom,fields,options){
            options = _.extend({
                    item_tag : "li",
                    disable : true,
                    showNamespace : true
                },
                options
            );
            _.each(fields,function(item){
                var edit;
                item = _.extend( {help:false,value : '',options:[],ns:''},item);

                var li = dcms.createControl(item,options);

                if (li)
                    dom.append(li);

            });
        },

        createPanel : function(dom ,fields, options){
            options = _.extend({
                    container_id : "editwindowConfigure",
                    container_style : "display:none;padding:60px;   width:50%;",
                    container_tag : 'ul'
                },
                options
            );
            var div = $('<'+options.container_tag+' id="'+options.container_id+'" style="'+options.container_style+'"/>');


            dcms.addPanelItems(dom ,fields, options);

            dom.append(div);
        },







        /* adds a custom ui button to the midgard create toolbar */

        add_custom_ui_select : function(id, label, options, selected, enabled_callback, disabled_callback) {
            // create the custom button only if it is not already existing
            if ($("button#" + id).length > 0) {
                return;
            }

            var custom_button = dcms.create_select_ui(id, label, options, selected);


            custom_button.hover(function () {
                    $(this).addClass("ui-state-hover");
                },
                function () {
                    $(this).removeClass("ui-state-hover");
                });

            custom_button.bind("change", function (event) {
                var is_active = $(this).hasClass("ui-state-active");
                if (!is_active) {
                    enabled_callback.call(this);
                    $(this).addClass("ui-state-active");
                }
                else {
                    disabled_callback();
                    $(this).removeClass("ui-state-active");
                }
            });

// add the button, wrapped by custom_buttons div
            var wrapper = $("div.create-ui-toolbar-dynamictoolarea li." + custom_toolbar_class);
            if (wrapper.length == 0) {
                // insert after workflows holder
                wrapper = $("<li class='" + custom_toolbar_class + "'></li>").insertAfter(
                    $("div.create-ui-toolbar-dynamictoolarea li.create-ui-tool-workflowarea")
                );
            }

            custom_button.appendTo(wrapper);
        },

        create_select_ui : function (id, label, options, selected) {
            var  result = $(
                "<select role='button' class='chosen-select create-ui-btn ui-button ui-widget ui-corner-all ui-state-default ui-button-text-only' id='" + id + "'/>");
            result.append("<option value=''></option>");
            _.each(options, function (value, index) {
                var text;
                if (_.isObject(value))
                {
                    var pairs = _.pairs(value);
                    value = pairs[0][0];
                    text = pairs[0][1];
                } else if(!_.isArray(options)) {
                    text = value;
                    value = index;
                }
                if (text === undefined)
                   text = value;

                var option = $("<option/>");
                option.html(text);
                option.attr("value",value);

                if (selected === value) {
                    option.attr("selected",true);

                }
                result.append(option);
            });
            return result;

        }


    }

})(jQuery);