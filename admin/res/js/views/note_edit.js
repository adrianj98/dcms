(function () {
    "use strict";
    APP.Views.NoteEditView = Backbone.View.extend({
        // functions to fire on events
        events: {
            "click button.save": "save"
        },

        // the constructor
        initialize: function (options) {
            this.note = options.note;
        },

        save: function (event) {
            // this keeps the form from submitting
            event.stopPropagation();
            event.preventDefault();

            var data = {};
            data = dcms.getValues(data);


            // update our model with values from the form
            this.note.set(data);
            this.note.save();
            // we would save to the server here with
            // this.note.save();
            // redirect back to the index
            window.location.hash = "notes/"+APP.schema.name+"/index";
        },

        // populate the html to the dom
        render: function () {
            var self = this;
            self.$el.html(_.template($('#formTemplate').html() ,{schema: APP.schema}));
            self.$el.find('h2').text('Edit '+APP.schema.text.name_singular );
            var itemData = self.note.toJSON();
            var fields = _.clone(APP.schema.fields);
            _.each(fields,function(field,index){
                fields[index].value = itemData[field.ns];
                if (itemData['_inherited'] && itemData['_inherited'][field.ns])
                fields[index].inherited  = itemData['_inherited'][field.ns];

            });
            dcms.addPanelItems(self.$el.find('#editForm'),fields ,{

                item_tag : 'div',
                item_class : 'form-group',
                disable : false,
                showNamespace : false
            });
            return this;
        }
    });
}());