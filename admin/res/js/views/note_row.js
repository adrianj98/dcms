(function (APP,Backbone,_,$) {

  APP.Views.NoteRowView = Backbone.View.extend({
    // the wrapper defaults to div, so only need to set this if you want something else
    // like in this case we are in a table so a tr
    tagName: "tr",
    // functions to fire on events
    events: {
        "click a.delete": "destroy",
        "click a.clone": "clone_item"
    },

    // the constructor
    initialize: function (options) {
      // model is passed through
      this.note  = options.note;
      this.notes = options.notes;
    },

    // populate the html to the dom
    render: function () {
        var self = this,
            data = this.note.toJSON();
        var exid = data[APP.schema.extends_id];
        self.$el.html(_.template($('#rowTemplate').html(), _.extend({schema: APP.schema,exid:exid},data)));
        var hasNoLinkField = _.where(APP.schema.fields, {clickable: true}).length === 0;
        _.each(APP.schema.fields,function(field){
            if (field.list){
                var value = dcms.getDisplay(data,field);
             if (hasNoLinkField || (field.clickable)){
                 value = '<a href="#note/'+APP.schema.name+'/'+data.id+'/edit">'+value+'</a>';
                 hasNoLinkField = false;
             }
            $('<td>'+value+'</td>').insertBefore(self.$el.find('.deleterow'));
            }
        });
      return self;
    },
      // delete the model
      clone_item: function (event) {
          event.preventDefault();
          event.stopPropagation();
          // we would call

          // which would make a DELETE call to the server with the id of the item
          this.notes.clone_item(this.note,{success:function(){
              notes.fetch({reset: true});
          }});

      }  ,
      // delete the model
      inherit_item: function (event) {
          event.preventDefault();
          event.stopPropagation();
          // we would call

          // which would make a DELETE call to the server with the id of the item
          this.notes.inherit_item(this.note,{success:function(){
              notes.fetch({reset: true});
          }});

      }  ,
    // delete the model
    destroy: function (event) {
      event.preventDefault();
      event.stopPropagation();
      // we would call
       this.note.destroy();
      // which would make a DELETE call to the server with the id of the item
      this.notes.remove(this.note);
      this.$el.remove();
    }
  });
})(APP,Backbone,_,jQuery);
