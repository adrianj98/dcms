/*global APP:true, _:true, jQuery:true, Backbone:true, JST:true, $:true*/
/*jslint browser: true, white: false, vars: true, devel: true, bitwise: true, debug: true, nomen: true, sloppy: false, indent: 2*/

(function () {
    "use strict";
    window.APP = window.APP || {Routers: {}, Collections: {}, Models: {}, Views: {}};
    APP.Views.NoteIndexView = Backbone.View.extend({
        // the constructor
        initialize: function (options) {
            // model is passed through
            this.notes = options.notes;
            this.notes.bind('reset', this.addAll, this);
        },

        // populate the html to the dom
        render: function () {
            var self = this;
            self.$el.html(_.template($('#indexTemplate').html(), {schema: APP.schema}));

            self.addAll();

            self.$el.find('#listSectionTabs li').first().addClass('active');
            self.$el.find('#listSections div').first().addClass('active');
            if (self.$el.find('#listSectionTabs li').length === 1) {
                self.$el.find('#listSectionTabs li').remove();
            }

            return this;
        },

        addAll: function () {
            // clear out the container each time you render index
            this.$el.find('tbody').children().remove();
            _.each(this.notes.models, $.proxy(this, 'addOne'));
        },

        addOne: function (note) {
            var view = new APP.Views.NoteRowView({notes: this.notes, note: note});
            var group = note.get('group') || 'General';
            var group_id = group.replace(/([^a-zA-z0-1])/, '');
            var tab = this.$el.find('#TabContent' + group_id);
            if (tab.length === 0) {
                this.$el.find('#listSectionTabs').append(_.template($('#newTabTemplate').html(), {group: group, group_id: group_id, data: note, schema: APP.schema}));
                this.$el.find('#listSections').append(_.template($('#newTabContentTemplate').html(), {group: group, group_id: group_id, data: note, schema: APP.schema}));
                var tab = this.$el.find('#TabContent' + group_id);

                _.each(APP.schema.fields, function (field) {
                    if (field.list) {


                        $('<td>' + field.caption + '</td>').insertBefore(tab.find('th.deleterow'));
                    }
                });
            }

            tab.find('tbody').append(view.render().el);
        }
    });
}());
