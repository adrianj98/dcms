(function () {
  "use strict";
  APP.Views.NoteNewView = Backbone.View.extend({
    // functions to fire on events
    events: {
      "click button.save": "save"
    },

    // the constructor
    initialize: function (options) {
      this.note  = options.note;
      this.notes = options.notes;
      this.note.bind('invalid', this.showErrors, this);
    },

    showErrors: function (note, errors) {
      this.$el.find('.error').removeClass('error');
      this.$el.find('.alert').html(_.values(errors).join('<br>')).show();
      // highlight the fields with errors
      _.each(_.keys(errors), _.bind(function (key) {
        this.$el.find('*[name=' + key + ']').parent().addClass('error');
      }, this));
    },

    save: function (event) {
      event.stopPropagation();
      event.preventDefault();
        var data = {};

        data = dcms.getValues(data);
        data.id = 0;//APP.createId();
      // update our model with values from the form

      this.note.set(data);
      if (this.note.isValid()){
        // add it to the collection
        this.notes.add(this.note);
         this.note.save();
        // redirect back to the index
        window.location.hash = "notes/"+APP.schema.name+ "/index";
      }
    },

    // populate the html to the dom
    render: function () {
       var self = this;

        self.$el.html(_.template($('#formTemplate').html(),{schema: APP.schema}) );
        this.$el.find('h2').text('Create New '+APP.schema.text.name_singular );
        var fields = APP.schema.fields;
        var itemData = self.note.toJSON();
        _.each(fields,function(field,index){
            if (itemData[field.ns])
                   fields[index].value = itemData[field.ns];


        });
        dcms.addPanelItems(self.$el.find('#editForm'),fields ,{


            item_tag : 'div',
            item_class : 'form-group',
            disable : false,
            showNamespace : false
        });
      return this;
    }
  });
}());
