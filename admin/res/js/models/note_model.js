(function () {

    APP.Models.NoteModel = Backbone.Model.extend({

        //  urlRoot : '/books',
        // you can set any defaults you would like here
        /*  defaults: {
         title: "",
         description: "",
         author: ""
         },*/


        validate: function (attrs) {
            var errors = {};

            if (!_.isEmpty(errors)) {
                return errors;
            }
        }
    });

    APP.schema = {};

    APP.Collections.NoteCollection = Backbone.Collection.extend({

        clone_item: function (note, options) {
            Backbone.sync('clone', note, options);
        },
        inherit_item :  function (note, options) {
            Backbone.sync('inherit', note, options);
        },
        // Reference to this collection's model.
        confirmCollection: function (collection, callback) {
            variant_id = collection;

            notes.url = 'api/admin/collection/' + collection;
            if (APP.schema && (APP.schema.name == 'collection')) {
                callback();
            } else {
                notes.fetch({reset: true, success: function () {
                    callback();
                }});
            }
        },
        model: APP.Models.NoteModel
    });
    var methodMap = {
        'create': 'POST',
        'update': 'PUT',
        'patch': 'PATCH',
        'delete': 'DELETE',
        'read': 'GET',
        'clone': 'OPTIONS',
        'inherit': 'OPTIONS'
    };
    var defaultSchema = {
        "description": "",
        "text": {
            "name_singular": "",
            "name_plural": ""
        }
    };
    Backbone.sync = function (method, model, options) {
        var type = methodMap[method];


        // Default JSON-request options.
        var params = {type: type, dataType: 'json',

            url: _.result(model, 'url'),

            success: function (resp) {
                if (resp.schema)
                    APP.schema = _.defaults(resp.schema, defaultSchema);
                if (resp.items) {
                    _.each(resp.items, function (item) {
                        if (!item.id) {
                            item.id = APP.createId();
                        }
                    })
                }
                var item = resp.items ? resp.items : resp.item;

                options.success(item);


            }
//         dataType: 'json'
        };


        // Ensure that we have the appropriate request data.
        if (options.data == null && model && (method === 'create' || method === 'update' || method === 'patch' || method || 'clone' || method || 'clone')) {
            params.contentType = 'application/json';
            params.data = JSON.stringify(options.attrs || model.changed);
        }


        // Don't process data on a non-GET request.
        if (params.type !== 'GET') {
            params.processData = false;
        }
        if (params.type === 'OPTIONS') {
            params.url = params.url + '/' + method;
        }


        $.ajax(params);
    };
    Backbone.ajaxs = function () {
        var success = arguments.success;
        arguments.success = function (resp) {
            if (resp.schema)
                APP.schema = _.defaults(resp.schema, defaultSchema);
            if (resp.items) {
                _.each(resp.items, function (item) {
                    if (!item.id) {
                        item.id = APP.createId();
                    }
                })
            }
            var item = resp.items ? resp.items : resp.item;

            success(item);


        }
        return Backbone.$.ajax.apply(Backbone.$, arguments);
    };
    APP.createId = function () {
        return (Math.floor(Math.random() * 100000000000) + 1).toString(36) + (Math.floor(Math.random() * 100000000000) + 1).toString(36);
    }

}());
