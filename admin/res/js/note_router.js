var variant_id = '';
(function () {

    window.APP = window.APP || {Routers: {}, Collections: {}, Models: {}, Views: {}};
    APP.Routers.NoteRouter = Backbone.Router.extend({
        routes: {
            "note/:collection/new": "create",
            "note/:collection/inherit/:id": "inherit",
            "notes/:collection/index": "index",
            "note/:collection/:id/edit": "edit",
            "note/:collection/:id/view": "show"
        },

        initialize: function (options) {
            this.notes = options.notes;
            // this is debug only to demonstrate how the backbone collection / models work
            this.notes.bind('reset', this.updateDebug, this);
            this.notes.bind('add', this.updateDebug, this);
            this.notes.bind('remove', this.updateDebug, this);
            //this.index();
        },

        updateDebug: function () {
            $('#output').text(JSON.stringify(this.notes.toJSON(), null, 4));
            // .animate({scrollTop: $('#offset').scrollHeight}, 1000);
        },

        create: function (collection) {
            var self = this;
            notes.confirmCollection(collection, function () {
                var newNote = new APP.Models.NoteModel();

                self.currentView = new APP.Views.NoteNewView({notes: self.notes, note: newNote});
                $('#primary-content').html(self.currentView.render().el);
            });
        },
        inherit: function (collection, id) {
            var self = this;
            notes.confirmCollection(collection, function () {
                var newNote = new APP.Models.NoteModel();
                var attr = {};
                attr['extends_id'] = id;
                newNote.set(attr);
                self.currentView = new APP.Views.NoteNewView({notes: self.notes, note: newNote});
                $('#primary-content').html(self.currentView.render().el);
            });
        },
        edit: function (collection, id) {
            var self = this;
            notes.confirmCollection(collection, function () {
                var note = self.notes.get(id);
                self.currentView = new APP.Views.NoteEditView({note: note});
                $('#primary-content').html(self.currentView.render().el);
            });
        },

        show: function (collection, id) {
            var self = this;
            notes.confirmCollection(collection, function () {
                var note = self.notes.get(id);
                self.currentView = new APP.Views.NoteShowView({note: note});
                $('#primary-content').html(self.currentView.render().el);
            });
        },

        index: function (collection) {
            var self = this;
            notes.confirmCollection(collection, function () {


                self.currentView = new APP.Views.NoteIndexView({notes: self.notes});
                $('#primary-content').html(self.currentView.render().el);
            });

            // we would call to the index with
            // this.notes.fetch()
            // to pull down the index json response to populate our collection initially
        }
    });
}());



