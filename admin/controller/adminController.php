<?php



class adminController extends baseController {

    public $guide = 'test';

  function index(){
      $cms= new CMS\helpers\DCms( $this->registry);
      if (!$this->registry->mongo->getSchema('users')->findOne())
           $cms->checkCollections();

      $cms->registerType('TDocList', new CMS\helpers\types\TDocList($cms));
      $cms->registerType('TDocSelect', new CMS\helpers\types\TDocSelect($cms));
      $cms->registerType('TDocBool', new CMS\helpers\types\TDocBool($cms));
      $cms->registerType('TDocText', new CMS\helpers\types\TDocText($cms));
      $cms->registerType('TDocDemandBase', new CMS\helpers\types\TDocDemandBase($cms));
      $cms->registerType('TDocJson', new CMS\helpers\types\TDocJson($cms));


      $colectionOnj = $this->registry->mongo->getSchema('collections')->find();
      $items = array();
      foreach($colectionOnj as $item){
          $schemaOnj = $this->registry->mongo->getSchema($item['schema']);
          $schemaItem = $schemaOnj->getSchema();
          if ($this->registry->user->canView($schemaOnj->getauth()))
                     $items[]= $schemaItem;

      }
      $this->registry->template->cms  = $cms;
      $this->registry->template->collections = $items;
      $this->registry->template->setView('main', "admin:index");
  }


    /**
     * gets a single product and returns by ajax
     *
     * @url GET /collection/$schema

     */
    function getCollection($schema){

        $schemaObj = $this->registry->mongo->getSchema($schema);
        if (!$this->registry->user->canView($schemaObj->getAuth()))
            return;


            $schemaArray = $schemaObj->getSchema();
            $query = $schemaArray['query']?:array();
            $colectionOnj = $schemaObj->find($query, array(),array('divInherited'=>true));
            $items = array();
            foreach($colectionOnj as $item){

                $items[]= $item;

            }

        $cms= new \CMS\helpers\DCms( $this->registry);

        $cms->updateSchema($schemaArray);
        return array('items'=> $items,'schema'=>$schemaArray);

    }
    /**
     * gets a single product and returns by ajax
     *
     * @url PUT /collection/$schema/$id

     */
    function putCollection($schema,$id,$data){
         $schemaObj = $this->registry->mongo->getSchema($schema);
        if (!$this->registry->user->canEdit($schemaObj->getAuth()))
            return;
        if ($id){

            unset($data['_id']);
            $unset = array();
            $set = array();
            foreach($data as $index => $field){

                if ($field == null)
                {

                    $unset[$index] = true;
                    unset($data[$index] );
                } elseif (strpos($field,'_') === 0){
                    unset($data[$index] );
                } else {
                    $set[$index] = $data[$index] ;
                }
            }
            $update = array();
            if (count($unset)){
                $update['$unset'] =  $unset;
            }
            if (count($set)){
                $update['$set'] =  $set;
            }

            $result = $schemaObj->update(array('id'=>$id),$update);
        }
        else
        {
            foreach($data as $index => $field){

                if ($field == null)
                {

                    $unset[$index] = true;
                    unset($data[$index] );
                } elseif (strpos($field,'_') === 0){
                    unset($data[$index] );
                }
            }
            $data['id'] = (string)$this->registry->mongo->createId();
            $result = $schemaObj->insert($data);
        }

        return array('item'=>$data);
    }
    /**
     * gets a single product and returns by ajax
     *
     * @url DELETE /collection/$schema/$id

     */
    function deleteCollection($schema,$id){
        $schemaObj = $this->registry->mongo->getSchema($schema);
        if (!$this->registry->user->canEdit($schemaObj->getAuth()))
            return;
        $result  = $schemaObj->remove(array('id'=>$id));


        return array('result'=>array('id'=>$id));
    }
    /**
     * gets a single product and returns by ajax
     *
     * @url OPTIONS /collection/$schema/$id/clone

     */
    function cloneCollection($schema,$id){
        $schemaObj = $this->registry->mongo->getSchema($schema);
        if (!$this->registry->user->canEdit($schemaObj->getAuth()))
            return;
        $result  = $schemaObj->cloneItem($id);


        return array('result'=>array('id'=>$id));
    }
    /**
     * gets a single product and returns by ajax
     *
     * @url OPTIONS /collection/$schema/$id/inherit

     */
    function inheritCollection($schema,$id){
        $schemaObj = $this->registry->mongo->getSchema($schema);
        if (!$this->registry->user->canEdit($schemaObj->getAuth()))
            return;
        $result  = $schemaObj->inheritItem($id);


        return array('result'=>array('id'=>$id));
    }
}
