<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 4/21/14
 * Time: 12:44 PM
 */

namespace CMS\models;
require_once (dirname(dirname(__FILE__)).'/DModels.php');


class DDummyCollection extends \CMS\models\DCollection {

}

namespace CMS\models\test;

class DModelsTest extends \PHPUnit_Framework_TestCase {
    private $schemaString = '
{

  "schema":
{
    "collection" : "DMongoCollectionTest",
    "query" : {"special": "thisSchema"},
    "type" : "dummy",
    "name" : "dummy",


    "fields": {
        "special" : {
            "default" : "thisSchema"
        },
        "id" : {
            "type": "TDocText",
            "caption" : "id",
            "ns" : "id",
            "default" : {"type":"random"}
        },

        "fieldOne": {
            "type": "TDocText",
            "list" : true,
            "editable" : true,
            "caption" : "field One",


            "default" : "jjj"
        },
        "fieldTwo": {

            "type": "TDocText",
            "list" : true,
            "editable" : true,
            "caption" : "Field Two"


        },
        "fieldThree": {
            "type": "TDocText",
            "list" : true,
            "editable" : true,
            "caption" : "field One",


            "default" : "bob"
        },
    "fieldFour": {
            "type": "TDocText",
            "list" : true,
            "editable" : true,
            "caption" : "field One",


            "default" :  {"type":"random"}
        }

    }

}

}
';
    function testInit(){
        $options = array('schemaPath' => dirname(__FILE__).'/schemas/', 'Dummy'=>'yes');
        $models = new \CMS\models\DModels();
        $models->init($options);
        $schema = json_decode($this->schemaString,true);
        $collection = $models->getSchema($schema);
        $this->assertEquals('yes',$collection->options);
        $collection = $models->getSchema('test');
        $schemaName = $collection->getSchema();
        $this->assertEquals('test',$schemaName['name']);
        $collection->install();

}

    function testMongoAvalible(){
        $models = new \CMS\models\DModels();
        $result = $models->mongoAvailable();
        $this->assertTrue($result);
    }
    function testCreateID(){
        $models = new \CMS\models\DModels();
        $result = $models->createId();
        $this->assertGreaterThan(6,strlen($result));
    }
}
 