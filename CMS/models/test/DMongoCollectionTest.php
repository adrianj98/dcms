<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 4/16/14
 * Time: 6:04 PM
 */

namespace CMS\models\test;

require_once (dirname(dirname(__FILE__)).'/DModels.php');
require_once (dirname(dirname(__FILE__)).'/DMongoCollection.php');

class DMongoCollectionTest extends \PHPUnit_Framework_TestCase {
     static $schema;
    function setup(){

    }
    function testCRUD(){

        $schema = json_decode(self::$schema,true);
        $collection = new \CMS\models\DMongoCollection($schema,array('url'=>'mongodb://localhost/?w=0','db'=>'testDB'));
        $collection->remove(array());
        $collection->insert(array('fieldOne'=>"55"));
        $data = $collection->find();
        $this->assertEquals('HEAD',$data[0]['_branch']);
        $collection->insert(array('fieldOne'=>"77"));
        $collection->update(array('fieldOne'=>"77"),array('$set'=>array("fieldTwo" => 'updated')));
        $data = $collection->findOne(array('fieldOne'=>"77"));
        $this->assertEquals('HEAD',$data['_branch']);
        $this->assertEquals('updated',$data['fieldTwo']);
        $this->assertEquals('bob',$data['fieldThree']);
        $this->assertGreaterThan(6,strlen($data['fieldFour']));
        $this->assertGreaterThan(6,strlen($data['id']));
        $data = $collection->findOne(array('fieldOne'=>"55"));
        $this->assertEquals('HEAD',$data['_branch']);
        $this->assertTrue(empty($data['fieldTwo']));
        $data = $collection->find();
        $this->assertCount(2,$data);
        $collection->remove(array('fieldOne'=>"55"));
        $data = $collection->find();
        $this->assertCount(1,$data);
        $this->assertEquals('updated',$data[0]['fieldTwo']);
        $collection->remove(array());
        $data = $collection->find();
        $this->assertCount(0,$data);

    }

    function testDBOptions(){
        $schema = json_decode(self::$schema,true);
        $collection = new \CMS\models\DMongoCollection($schema,array('url'=>'mongodb://localhost/?w=0','db'=>'testDB'));
        $collection->remove(array());
        $collection->insert(array('fieldOne'=>"55"));
        $collection = new \CMS\models\DMongoCollection($schema,array( 'db'=>'testDB'));

        $data = $collection->find();

        $this->assertEquals('HEAD',$data[0]['_branch']);
    }
    function testClone(){
        $schema = json_decode(self::$schema,true);
        $collection = new \CMS\models\DMongoCollection($schema,array('url'=>'mongodb://localhost/?w=0','db'=>'testDB'));
        $collection->remove(array());
        $id =$collection->insert(array('data1'=>'ffff','fieldTwo'=>'bob'));
        $data1 = $collection->findOne(array('id'=>$id));
        $id2 = $collection->cloneItem($id);
        $data2 = $collection->findOne(array('id'=>$id2));
        $this->assertEquals($data1['data1'], $data2['data1']);
        $this->assertEquals($data1['fieldTwo'], $data2['fieldTwo']);
        $this->assertEquals($data1['fieldOne'], $data2['fieldOne']);

    }

}

DMongoCollectionTest::$schema =  '
{    "schema":
{
    "collection" : "DMongoCollectionTest",
    "query" : {"special": "thisSchema"},



    "fields": {
        "special" : {
            "default" : "thisSchema"
        },
        "id" : {
            "type": "TDocText",
            "caption" : "id",
            "ns" : "id",
            "default" : {"type":"random"}
        },

        "fieldOne": {
            "type": "TDocText",
            "list" : true,
            "editable" : true,
            "caption" : "field One",


            "default" : "jjj"
        },
        "fieldTwo": {

            "type": "TDocText",
            "list" : true,
            "editable" : true,
            "caption" : "Field Two"


        },
        "fieldThree": {
            "type": "TDocText",
            "list" : true,
            "editable" : true,
            "caption" : "field One",


            "default" : "bob"
        },
    "fieldFour": {
            "type": "TDocText",
            "list" : true,
            "editable" : true,
            "caption" : "field One",


            "default" :  {"type":"random"}
        }

    }

}

}
';