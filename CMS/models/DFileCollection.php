<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 1/27/14
 * Time: 12:58 PM
 */

namespace CMS\models;


class DFileCollection  extends DSchemaCollection{
    function __construct($items, $schema = null, $options = array())
    {
        parent::__construct($schema, $options);
        $name = $this->schema['file'];
        $this->items = json_decode(file_get_contents(\DCore::getFilePath('protected:data/'.$name, 'config', '', '.json')), true);
    }
}
