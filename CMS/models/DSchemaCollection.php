<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 1/27/14
 * Time: 12:39 PM
 */

namespace CMS\models;

/**
 * Class DSchemaCollection
 * @package CMS\models
 */
class DSchemaCollection extends DCollection
{
    protected
        $items;

// The main compilation function for a given selector.





    function __construct(  $schema = null, $options = array())
    {
        parent::__construct($schema, $options);
        $this->items = $schema['items'];
    }

    function find($query = array(), $fields = array())
    {
        $result = array();
        $selector = new \CMS\helpers\DMongoSelector();
        if (count($query) == 0)
            return ($this->items);
        foreach ($this->items as $record) {
            $output = $selector->compileDocumentSelector($query, $record);
            if ($output) {
                $result[] = $record;
            }
        }
        return $result;
    }

    function findOne($query = array(), $fields = array())
    {
         $result = $this->find($query,$fields);
        return $result[0];
    }

    function update($query, $update, $options = array())
    {
        return;
    }

    function insert($data, $options = array())
    {
        return;
    }

    function remove($query = array(), $options = array())
    {
        return;
    }
}