<?php
/**
 * User: adrianjones
 * Date: 10/18/13
 * Time: 8:08 PM
 */

namespace CMS\models;

/**
 * Class DModels
 * @package CMS\models
 *
 * DModels is a factory for collection schema object
 *
 */
class DModels {

    private $_options;
    private $_schemaPath;
    static function mongoAvailable(){
        return extension_loaded('mongo');
    }

    /**
     * sets options of instances of collections
     *
     * options are passed to collections by collection class name  `$this->_options[$class] ` is pass to the
     * collections create function
     *
     * @param $options
     */
    function init($options){
        $this->_schemaPath = isset($options['schemaPath'])?$options['schemaPath']:'';
        $this->_options = $options;

    }

    /**
     * @param $schema string|array
     * @return DCollection
     */
    function getSchema($schema){

        if (!is_array( $schema)){

        $schemaObj = json_decode(file_get_contents($this->_schemaPath .'/'.$schema .'.json'), true);

        if (empty($schemaObj['schema']))
            $schemaObj = array('schema' => $schemaObj);
        $schemaObj['schema']['name'] = $schema;
        $schema = $schemaObj;

        }
        $class =  isset($schema['schema']['type'])?ucfirst($schema['schema']['type']):'Mongo';

        $collectionClass = "CMS\\models\\D".$class."Collection";

        $collection = new $collectionClass($schema,$this->_options[$class],$this);

        return $collection;

    }
    function createId($id = null){
        return new \MongoId($id);
    }


}

class DCollection {

    static $branch = 'HEAD';
    protected $schema;
    public $options;
    protected $factory;
    function getSchema(){
        return $this->schema;
    }

function install(){
    $filename = $this->factory->_schemaPath .'/../default/'.$this->schema['name'] .'.json';
    if ($filename){
    $data = file_get_contents($filename);
    $data = json_decode($data,true);
    $this->_install($data);
    }
}
    function _install($data){

    }
    function getAuth(){
        $auth = $this->schema['auth'];
        $view = $this->schema['view'] ? : $auth;
        if (!is_array($auth))
            $auth = explode(',', $auth);
        if (!is_array($view))
            $view = explode(',', $view);
        return array('auth' => $auth, 'view' => $view);
    }
    function __construct($schema,$options = array(),$factory = null){

        $this->factory = $factory;
        foreach($schema['schema']['fields'] as $key => &$sch){
            $sch['ns'] = isset($sch['ns'])?$sch['ns']:$key;
        }
        $this->schema = $schema['schema'];
        $this->options = $options;
    }
    function getDefaultValue($default){
        if (is_string($default))
            return $default;
        if ($default['type'] === 'random'){
            return md5(rand());
        }
        if ($default['type'] === 'key'){
            return 'vvvv';
        }
    }
    function sanitise($data){
        foreach($this->schema['fields'] as $field){
            if (isset($field['default']))
                if (!isset($data[$field['ns']]))
                    $data[$field['ns']] = $this->getDefaultValue($field['default']);
        }
        return $data;
    }
    function find($query = array(),$fields = array(),$options=array()){
        return  $this->_find($query,$fields,$options);
    }
    function findOne($query = array(),$fields = array(),$options=array()){
        return  $this->_findOne($query,$fields,$options);
    }
    function update($query,$update,$options = array()){
        return  $this->_update($query,$update,$options);
    }

    function remove($query = array(),$options = array()){
        return  $this->_remove($query,$options);
    }
    function cloneItem($id){
        return  $this->_cloneItem($id);
    }
    function inheritItem($id){
        return $this->_inheritItem($id);
    }
    function insert($data,$options = array()){
        $data =$this->sanitise($data);
        return $this->_insert($data,$options);
    }
}
