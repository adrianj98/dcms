<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 1/27/14
 * Time: 12:40 PM
 */

namespace CMS\models;

/**
 * Class DMongoCollection
 * @package CMS\models
 */
class DMongoCollection extends DCollection
{

    private $collection = null;
    static protected $_db;

    function getCollection($collection, $options)
    {
        $dbname = isset($options['db'])?$options['db']:null;
        if (empty(self::$_db[$dbname])) {
            $m = new \MongoClient($options['url']); // connect
            $db = $m->selectDB($dbname);
            self::$_db[$dbname] = $db;
        } else
            $db = self::$_db[$dbname];

        $name = $collection;
        return $db->$name;

    }

    function __construct($schema = null, $options = array())
    {
        parent::__construct($schema, $options);

        $this->collection = $this->getCollection($schema['schema']['collection'], $options);
    }

    function _cloneItem($id){
        $item = $this->findOne(array('id'=>$id));
        $item['id'] = md5(rand());
        unset($item['_id']);
        $id =$this->insert($item);
        return $id;
    }
    function _inheritItem($id){
        $newItem = array($this->schema['extends_id'] = $id);
        foreach($this->schema['fields'] as $field){
            if (!empty($field['clickable'])){
                $newItem[$field['ns']] = 'Inheited from '.$id;
                break;
            }

        }
        $id =$this->insert($newItem);
        return $id;
    }
    function getInheritance(&$item, $fields = array(), $options = array())
    {
        if (empty($this->schema['extends_id']) ||
            (empty($options['divInherited']) && (empty($options['mergeInherited'])))
        ) {
            return array();
        }
        $parent = array('extends_id' => $item['extends_id']);
        $inheritedList = array();
        $inherited = array();
        if ($options['mergeInherited'])
            $inherited = $item;
        while (isset($parent['extends_id'])) {
            if (!in_array($inheritedList, $parent['extends_id'])) {
                $parent = $this->collection->findOne(array($this->schema['extends_id'] => $parent['extends_id']), $fields);
                if ($parent){
                   foreach($this->schema['fields'] as $ns => $field){
                       if (!empty($field['merge'])){
                           if (is_array($parent[$ns]) && is_array($inherited[$ns]))
                              $inherited[$ns] = array_merge($parent[$ns], $inherited[$ns]);
                       }
                   }
                   $inherited = array_merge($parent, $inherited);
                }
                $inheritedList[] = $parent['extends_id'];

            }
        }
        if ($options['divInherited'])
            $item['_inherited'] = $inherited;
        if ($options['mergeInherited'])
            $item  = $inherited;
    }


    function getBranchQuery($query,$branch){
        $versionQuery = array('$or'=> array( 0 => array('_branch' => 'HEAD'),1=> array( '_branch' => array('$exists'=>false))));
        $queryHead = array_merge($query , $versionQuery);
        $versionQuery =  array('_branch' => $branch);
        $queryBranch = array_merge($query , $versionQuery);
        return array('head'=>$queryHead,'branch'=>$queryBranch);

    }
    function _find($query = array(), $fields = array(), $options = array())
    {
        if (isset($this->schema['query']))
            $query = array_merge($this->schema['query'],$query);
        $queries = $this->getBranchQuery($query,self::$branch);

        $items = $this->collection->find($queries['branch'], $fields);
        $itemsHead = $this->collection->find( $queries['head'], $fields);

        $itemList = array();
        foreach($items as $item){
            $itemList[$item['id']] = $item;
        }
        foreach($itemsHead as $item){
            $itemList[$item['id']] = $item;
        }

        $resultItems = array();


            foreach ($itemList as $result) {
                if (!empty($options['divInherited']) || !empty($options['mergeInherited'])) {
                $this->getInheritance($result, $fields, $options);
                }
                $resultItems[] = $result;
            }




        return $resultItems;
    }

    function _findOne($query = array(), $fields = array(), $options = array())
    {
        $result = $this->collection->findOne($query, $fields);
        $this->getInheritance($result, $fields, $options);
        return $result;
    }

    function _update($query, $update, $options = array())
    {
        return $this->collection->update($query, $update);
    }

    function _insert($data, $options = array())
    {
        $data = array_merge($data,array('_branch'=>self::$branch));
        return $this->collection->insert($data, $options);
    }

    function _remove($query = array(), $options = array())
    {
        return $this->collection->remove($query);
    }

    function _install($data)
    {
        $query = $this->schema->fields['query'] ? $this->schema->fields['query'] : array();
        if (!$this->collection->findOne($query)) {

            foreach ($data as $itemKey => $item) {
                $default = array();
                foreach ($this->schema['fields'] as $key => $field) {
                    if ($field['default']) {
                        if (isset($field['default']['type'])) {
                            if ($field['default']['type'] == 'random')
                                $default[$key] = (string)DModels::createId();
                            elseif ($field['default']['type'] == 'key')
                                $default[$key] = $itemKey;
                            else
                                $default[$key] = $field['default'];
                        }
                    }
                }
                $item = array_merge($default, $item);

                $this->collection->insert($item);
            }
        }
    }
}
