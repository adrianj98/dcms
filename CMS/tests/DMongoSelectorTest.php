<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 3/21/14
 * Time: 11:24 AM
 */




class DMongoSelectorTest extends PHPUnit_Framework_TestCase {

    function testContent(){
    include dirname(__FILE__)."/../helpers/DMongoSelector.php";
       $selector = new \CMS\helpers\DMongoSelector();
       $result = $selector->compileDocumentSelector(array('apple' => 'red'),array('bob'=>'dfdf','apple'=>'red'));
       $this->assertTrue($result);
        $result = $selector->compileDocumentSelector(array('apple' => array('$gt'=>6)),array('bob'=>'dfdf','apple'=>9));
        $this->assertTrue($result);
        $result = $selector->compileDocumentSelector(array('apple' => array('#lt'=>6)),array('bob'=>'dfdf','apple'=>9));
        $this->assertFalse($result);
        $result = $selector->compileDocumentSelector(array('apple' => array('#lt'=>6)),array('bob'=>'dfdf','apple'=>3));
        $this->assertTrue($result);
        $result = $selector->compileDocumentSelector(array('$or' => array('apple'=>3,'bob'=>56)),array('bob'=>'dfdf','apple'=>3));
        $this->assertTrue($result);
        $result = $selector->compileDocumentSelector(array('apple' => array('$in'=>array(3,6,8))),array('bob'=>'dfdf','apple'=>3));
        $this->assertTrue($result);
        $result = $selector->compileDocumentSelector(array('apple' => array('$in'=>array(83,6,8))),array('bob'=>'dfdf','apple'=>3));
        $this->assertFalse($result);
        $select = json_decode('{"$or" : [{"primary_sic":{"#in":["6141","6061","6062"]}},{"ds_industry":{"#in":["Credit Unions","Insurance","Healthcare/Medical","Government","Financial Services","Technology","Education","HR Staffing"]},"annual_sales":{"#gt":100000000}}] }',true);
        $data = json_decode('{
    "registry_company_name": "CenturyLink",
    "registry_city": "Sunnyvale",
    "registry_state": "CA",
    "registry_zip_code": null,
    "registry_area_code": 408,
    "registry_country": "United States",
    "registry_country_code": "US",
    "registry_latitude": 37.368801116943,
    "registry_longitude": -122.03630065918,
    "company_name": "Linkedin Corporation",
    "demandbase_sid": 2070916,
    "marketing_alias": "Linkedin",
    "industry": "Software & Technology",
    "sub_industry": "Data & Technical Services",
    "employee_count": 3458,
    "isp": false,
    "primary_sic": "7374",
    "street_address": "2029 Stierlin Ct Ste 200",
    "city": "Mountain View",
    "state": "CA",
    "zip": "94043",
    "country": "US",
    "country_name": "United States",
    "phone": "650-687-3600",
    "stock_ticker": "LNKD",
    "web_site": "linkedin.com",
    "annual_sales": 836430000,
    "revenue_range": "$500M - $1B",
    "employee_range": "Enterprise",
    "b2b": true,
    "b2c": false,
    "traffic": "Very High",
    "latitude": 37.4232,
    "longitude": -122.075,
    "fortune_1000": false,
    "forbes_2000": true,
    "information_level": "Detailed",
    "audience": "Mid-Market Business",
    "audience_segment": "Software & Technology",
    "ip": "65.113.36.85",
    "registry_dma_code": 807,
    "registry_country_code3": "USA",
    "ds_industry": "Technology"
  }',true);
        $result = $selector->compileDocumentSelector($select ,$data);
        $this->assertFalse($result);

        $select = array('t'=> array('$NOW'=>0));
        $result = $selector->compileDocumentSelector($select ,$data);
        $this->assertTrue($result);


    }
}
 