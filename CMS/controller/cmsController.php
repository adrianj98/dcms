<?php

class cmsController extends baseController {

    function index(){

    }
    /**
     * gets a single product and returns by ajax
     *
     * @url POST /update
     * @url PUT /update
     */
    public function updateAction($data) {

        $post = array();
       foreach($data as $key => $dateItem){


           $key = str_replace('[<http://viejs.org/ns/root.','=',$key);
           $key = str_replace('>]','',$key);
           $item = explode('=',$key);
           if(trim($item[1]))
           $post[$item[1]] = $dateItem;

       }

       // $data = $this->getPost();
      //  DCore::using('root:api/rest/v2/DocuSignAPI');

        $cms = new CMS\helpers\DCms( $this->registry);
        $cms->editing = true;
        $id = trim($data['content[@subject]'],">");
        $id = trim($id,"<");
        $id = explode('^',$id);
        $schema = $this->registry->mongo->getSchema($id[0]);



        if (!$this->registry->user->canView($schema->getAuth()))
            return;


        $result =$schema->update(array('docId'=>$id[0],'variant_id'=>$id[1]),array('$set'=>$post));


        return array();




    }
    /**
     * gets a single product and returns by ajax
     *
     * @url POST /inherit
     * @url PUT /inherit
     */
    public function inheritAction($data) {

        // $data = $this->getPost();
        //  DCore::using('root:api/rest/v2/DocuSignAPI');
        $user = $_SESSION['CMS_USER'];
        if (empty($user['author']))
            return;
        $cms = new DCms( $this->registry);
        $cms->editing = true;


        $cms->load($data['document'],null);
        $cms->inheritFrom($data['parent_id'],$data['newName']);


        return array();




    }
    /**
     * gets a single product and returns by ajax
     *
     * @url POST /clone
     * @url PUT /clone
     */
    public function cloneAction($data) {

        // $data = $this->getPost();
        //  DCore::using('root:api/rest/v2/DocuSignAPI');
        $user = $_SESSION['CMS_USER'];
        if (empty($user['author']))
            return;
        $cms = new DCms( $this->registry);
        $cms->editing = true;


        $cms->load($data['document'],null);
        $cms->cloneFrom($data['parent_id'],$data['newName']);


        return array();




    }
    /*
     * url: '/code/api/cms/inherit',
                            data: {parent_id :variant_id , newName:variantname},
     */
    public function loginAction(){
        $username = $_POST['username'];
        if ($_POST['logout'])
        {
            $_SESSION['CMS_USER'] = null;
        } else if (isset($username)){
            $password = $_POST['password'];
            $user = $this->registry->mongo->getSchema('users')->findOne(array('username'=>$username, 'password' => $password));
            if (empty($user))
                die('invalid Login');
           if ($this->registry->user->login($user))
         DCore::redirect("admin");
        }

            //set templates
            $this->registry->template->setView('main', 'CMS:login');


    }
    /**
     * gets a single product and returns by ajax
     *
     * @url POST /createVariant
     */
    public function createVariant($data){
        $user = $_SESSION['CMS_USER'];
        if (empty($user['author']))
            return;
        $cms = new DCms( $this->registry);
        $cms->editing = true;
        $cms->load($data['docId']);
        $cms->createVariant($data['variantName']);


    }
}