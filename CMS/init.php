<?php

global $CONFIG;

$user = new \CMS\helpers\DCMSUser(registry, $this->registry->session);;
$registry->user = $user;

$registry->router->addService('CMS:cmsController', '/cms');
$registry->router->setController('cms','CMS:cms');

if (CMS\models\DModels::mongoAvailable()){
$schemaPath =  \DCore::getPathOfAlias('app').'/config/schemas/';
$registry->mongo = new  CMS\models\DModels();

$registry->mongo->init(array('Mongo'=>$CONFIG['mongo'],'schemaPath'=>$schemaPath));
}