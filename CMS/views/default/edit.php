<?php
/**
 * User: adrianjones
 * Date: 10/28/13
 * Time: 11:32 AM
 */


$this->addJS('CMS:res!/lib/jquery-ui/jquery-ui');
$this->addJS('CMS:res!/lib/underscore/underscore');
$this->addJS('CMS:res!/lib/backbone/backbone');
$this->addJS('CMS:res!/lib/vie/vie');
$this->addJS('CMS:res!/lib/jquery-rdfquery/jquery.rdfquery.core');
$this->addJS('CMS:res!/lib/jquery-rdfquery/jquery.rdfquery.rules');
$this->addJS('CMS:res!/lib/annotate/annotate');
$this->addJS('CMS:res!/lib/rangy/rangy-core');
$this->addJS('CMS:res!/lib/hallo/hallo');
$this->addJS('CMS:res!/create');

//$this->addCSS('CMS:res!/lib/jquery-ui/vader/jquery.ui');
//$this->addCSS('CMS:res!/lib/jquery-ui/vader/jquery.ui.theme');

$this->addCSS('CMS:res!/lib/font-awesome/css/font-awesome');
$this->addCSS('CMS:res!/themes/create-ui/css/create-ui');
$this->addCSS('CMS:res!/themes/midgard-notifications/midgardnotif');


$this->addJS('admin:res!/js/dcms');
$this->addJS('CMS:js/inlineEdit');
$this->addCSS('CMS:css/edit');


?>

<script>

    var variant_list = <?php echo json_encode($this->cms->getVariantIds()); ?>;
    var doc_id = <?php echo json_encode($this->cms->getId()); ?>;
    var variant_id = "<?php echo $this->cms->getVariantId() ?>";
    var configItems = <?php echo json_encode($this->cms->getSchema()); ?>;

    jQuery(function(){

        <?php echo $this->cms->renderJS(); ?>




      dcms.initCreateJs();
    });

</script>


<div id="variant-select-form" title="Create A Variant">
    <p class="validateTips">All form fields are required.</p>

    <form>
        <fieldset>
            <label for="name">Name</label>
            <input type="text" name="name" id="variant-name" class="text ui-widget-content ui-corner-all">

        </fieldset>
    </form>
</div>


