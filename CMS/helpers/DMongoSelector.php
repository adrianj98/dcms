<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 3/21/14
 * Time: 10:36 AM
 */

namespace CMS\helpers;


global $LOGICAL_OPERATORS, $COMPARISON_OPERATORS,$FUNCTIONS_OPERATORS;

$FUNCTIONS_OPERATORS = array(
    /*
     *    $NOW	return date timestamp.
     */
    '$NOW' => function ($value) {

            return  time();
        },

);
$COMPARISON_OPERATORS = array(

    /*
     *    $gt	Matches values that are greater than the value specified in the query.
     */
    '$gt' => function ($key, $value, $record, $collection) {
            if (!isset($record[$key])) return false;
            return $record[$key] > $value;
        },
    /*
     *    $gte	Matches values that are equal to or greater than the value specified in the query.
     */
    '$gte' => function ($key, $value, $record, $collection) {
            if (!isset($record[$key])) return false;
            return $record[$key] >= $value;
        },
    /*
     * $in	Matches any of the values that exist in an array specified in the query.
     */
    '$in' => function ($key, $value, $record, $collection) {
            if (!isset($record[$key])) return false;
            return in_array($record[$key], $value);
        },
    /*
     * $lt	Matches values that are less than the value specified in the query.
     */
    '$lt' => function ($key, $value, $record, $collection) {
            if (!isset($record[$key])) return false;
            return $record[$key] < $value;
        },
    /*
     * $lte	Matches values that are less than or equal to the value specified in the query.
     */
    '$lte' => function ($key, $value, $record, $collection) {
            return $record[$key] <= $value;
        },
    /*
     * $ne	Matches all values that are not equal to the value specified in the query.
     */
    '$ne' => function ($key, $value, $record, $collection) {
            return $record[$key] != $value;
        },
    /*
     * $nin	Matches values that do not exist in an array specified to the query.
     */
    '$nin' => function ($key, $value, $record, $collection) {
            if (!isset($record[$key])) return true;
            return !in_array($record[$key], $value);
        },

    /*
     *
    $all	Matches arrays that contain all elements specified in the query.
     */
    '$all' => function ($key, $value, $record, $collection) {
            if (!isset($record[$key])) return false;
            return count(array_diff($record[$key], $value)) == 0;
        },
    /*
     *
$elemMatch	Selects documents if element in the array field matches all the specified $elemMatch condition.
     */
    '$elemMatch' => function ($key, $value, $record, $collection) {
            foreach($value as $el)
                if (!in_array($el,$record[$key] ))
                    return false;
            return true;
        },
    /*
     *
$size	Selects documents if the array field is a specified size.
     */
    '$size' => function ($key, $value, $record, $collection) {
            return count($record[$key]) == $value;
        },


/*
 * $exists	Matches documents that have the specified field.
*/
    '$exists' => function ($key, $value, $record, $collection) {
            return isset($record[$key]);
        },

    /*
     * $type	Selects documents if a field is of the specified type.
    */

/*
 * $mod	Performs a modulo operation on the value of a field and selects documents with a specified result.
*/
/*
 * $regex	Selects documents where values match a specified regular expression.
*/
    '$exists' => function ($key, $value, $record, $collection) {
            return preg_match("/".$value."/",$record[$key]);
        },
/*
 * $text	Performs text search.
*/
/*
 * $where	Matches documents that satisfy a JavaScript expression.
*/
);
$LOGICAL_OPERATORS = array(
    /**
     *  $or    Joins query clauses with a logical OR returns all documents that match the conditions of either clause.
     */
    '$or' => function ($selector, $record, $collection) {
            $result = array();

                $collection->compileDocumentSelector($selector, $record,$result);
            return in_array(true, $result);
        },
    /**
     * $and    Joins query clauses with a logical AND returns all documents that match the conditions of both clauses..
     */
    '$and' => function ($selector, $record, $collection) {
            $result = array();

            return $collection->compileDocumentSelector($selector, $record);

        },
    /**
     * $not    Inverts the effect of a query expression and returns documents that do not match the query expression..
     */
    '$not' => function ($selector, $record, $collection) {
            $result = array();
            foreach ($selector as $subSelector)
                $result[] = $collection->compileDocumentSelector($subSelector, $record);
            return !in_array(true, $result);
        },
    /**
     * $nor    Joins query clauses with a logical NOR returns all documents that fail to match both clauses..
     */
    '$nor' => function ($selector, $record, $collection) {
            $result = array();
            foreach ($selector as $subSelector)
                $result[] = $collection->compileDocumentSelector($subSelector, $record);
            return !in_array(false, $result);
        }


);

class DMongoSelector {

    function getSubSelector($docSelector, $key, $record)
    {
        global $COMPARISON_OPERATORS,$FUNCTIONS_OPERATORS;
        if (is_array($docSelector)) {

            foreach ($docSelector as $op => $value) {
                if (substr($op, 0, 1) === '#') {
                    $op[0] = '$';
                }
                if (substr($op, 0, 1) === '$') {
                    // Outer operators are either logical operators (they recurse back into
                    // this function), or $where.

                    if (isset($FUNCTIONS_OPERATORS[$op])) {
                        return   $record[$key] ==  $FUNCTIONS_OPERATORS[$op]($value);
                    } else {

                        if (!isset($COMPARISON_OPERATORS[$op]))
                             throw new \Exception("Unrecognized logical operator: " . $op);
                        return $COMPARISON_OPERATORS[$op]($key, $value, $record, $this);

                    }
                }
            }
        } else {
            //  $lookUpByIndex = LocalCollection._makeLookupFunction(key);
            return $record[$key] == $docSelector;

        }

    }

    function compileDocumentSelector($docSelector, $record,&$result = array())
    {

        global $LOGICAL_OPERATORS;
        foreach ($docSelector as $key => $subSelector) {
             if (substr($key, 0, 1) === '#'){
               $key[0] = '$';
             }
            if (substr($key, 0, 1) === '$') {
                // Outer operators are either logical operators (they recurse back into
                // this function), or $where.
                if (!isset($LOGICAL_OPERATORS[$key]))
                    throw new \Exception("Unrecognized logical operator: " . $key);
                $result[] = $LOGICAL_OPERATORS[$key]($subSelector, $record, $this);
            } else
            {
                //  $lookUpByIndex = LocalCollection._makeLookupFunction(key);
                $result[] = $this->getSubSelector($subSelector, $key, $record);
            }
        }
        return !in_array(false, $result);
    }

}