<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 1/17/14
 * Time: 3:53 PM
 */

namespace CMS\helpers;

class DCMSRouteLogic {



    private $cache = null;
    protected function collection(){
        global $registry;
        $routes = $registry->mongo->getSchema('routes')->find();
        foreach($routes as $route){
            $result[]= $route;
        }
        return $result;
    }
    /** @var array */
    private $_wildcardTypes = array(
        'num'    => '([0-9]+)',
        'alpha'  => '([0-9A-Za-z_\-]+)',
        'hex'    => '([0-9A-Fa-f]+)',
        'base64' => '([0-9A-Za-z+/=.\-_]+)',
        'query'  => '\?(.*?)',
        'all'    => '*(.*?)',
    );
    /**
     * @return array
     */
    protected function _getWildcardTypes()
    {
        return $this->_wildcardTypes;
    }
    function __construct($options = null){

        $this->cache =  $options['cache'];
        $this->collection =  $options['collection'];


    }
    // ##########################################

    /**
     * @param $route
     * @return string
     */
    protected function _compileRoute($route)
    {
        $wildcardTypes = $this->_getWildcardTypes();

        foreach($wildcardTypes as $type => $regexp)
        {
            $route = str_replace(':' . $type, $regexp, $route);
        }
        if (substr($route,0,1) !== '/'){
        // make sure we got a leading slash
      //  $route = '/' . ltrim($route, '/');

        // add final slash
     //   $route .= '/*';

        // escape fwd slash
        $route = str_replace('/', '\/', $route);
        $route =  "/^$route$/";
        };

        return $route;
    }
    function getRoutes(){
       // $routes = $this->cache->get('DCMSRouteLogic:routes');

        if (empty($routes)){
            $routes = array();


                $collection = $this->collection();
                $routes = array_merge($routes,$collection);

            $this->cache->set('DCMSRouteLogic:routes',$routes, isset(\cache\base::$shortTime)?(\cache\base::$shortTime):0);
        }
        return $routes;
    }
    function getRoute($routeUri){
        global $registry;
       $routes = $this->getRoutes();
        $d_base = new  DDemandBase();


      //  $id = (isset($registry->request)) ? $registry->request->query->get('force_route') : null;
        $id = $_GET['force_route'];
        if (  $id )
        foreach ($routes as $route) {
            if (($id) && ($id ==     $route['id']))
                return $route;
        }
        foreach($routes as  $route){

             $pattern = $this->_compileRoute($route['pattern']) ;
            if (preg_match($pattern,$routeUri)){
                $route['server_path'] = $routeUri;
                if (empty($route['demandBase']) || ($d_base->query($route['demandBase'])))
                          return $route;
            }


        }



    }
    function processRoute($route){
       if (is_string($route) || (empty($route)))
           $route =  $this->getRoute($route);
       while ($route['class']){
           $routeClass = ('DRouter'.$route['class']);
         $routerObj = new $routeClass($route);
         $route = $routerObj->getRoute();
       }

        return $route;

    }
} 