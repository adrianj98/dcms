<?php
/**
 * User: adrianjones
 * Date: 8/14/13
 * Time: 11:53 AM
 */

namespace CMS\helpers;

class DDemandBase {
    static $_countryCodes =  array(
        'al' =>  'Albania',
        'ad' => 'Andorra',
        'at' => 'Austria',
        'by' => 'Belarus',
        'be' => 'Belgium',
        'ba' => 'Bosnia and Herzegovina',
        'bg' => 'Bulgaria',
        'hr' => 'Croatia',
        'cz' => 'Czech Republic',
        'dk' => 'Denmark',
        'ee' => 'Estonia',
        'fi' => 'Finland',
        'fr' => 'France',
        'de' => 'Germany',
        'gr' => 'Greece',
        'va' => 'Holy See (Vatican City)',
        'hu' => 'Hungary',
        'is' => 'Iceland',
        'ie' => 'Ireland',
        'it' => 'Italy',
        'xk' => 'Kosovo',
        'lv' => 'Latvia',
        'li' => 'Liechtenstein',
        'lt' => 'Lithuania',
        'lu' => 'Luxembourg',
        'mk' => 'Macedonia',
        'mt' => 'Malta',
        'md' => 'Moldova',
        'mc' => 'Monaco',
        'me' => 'Montenegro',
        'nl' =>'Netherlands',
        'no' => 'Norway',
        'pl' => 'Poland',
        'pt' => 'Portugal',
        'ro' =>'Romania',
        'sm' => 'San Marino',
        'rs' => 'Serbia',
        'sk' => 'Slovakia',
        'si' => 'Slovenia',
        'es' => 'Spain',
        'se' => 'Sweden',
        'ch' => 'Switzerland',
        'ua' => 'Ukraine',
        'gb' => 'United Kingdom'
    );

    static $_industryMap = array(
        "Aerospace & Defense"        => "Manufacturing",
        "Agriculture"                => array("Machinery" => "Manufacturing", 'default' => 'Other'),
        "Apparel"                    => array("Retail" => "Retail", 'default' => 'Manufacturing'),
        "Associations"               => "Not For Profit",
        "Automotive"                 => array("Dealers" => "Retail", "Retail" => "Retail", 'default' => 'Manufacturing'),
        "Biotech"                    => "Healthcare/Medical",
        "Business Services"          => array("Accounting" => "Accounting", "Employment" => "HR Staffing",
                                              "Legal"      => "Legal", "Architectural" => "Architect",
                                              'default' => 'Business Services'),
        "Construction"               => "Construction",
        "Consumer Goods & Services"  => "Other",
        "Education"                  => "Education",
        "Electronics"                => "Technology",
        "Energy & Utilities"         => "Utilities",
        "Financial Services"         => array("Banking & Finance"    => "Banking",
                                              "Insurance" => "Insurance",
                                              "Real Estate Services" => "Real Estate - Property Management",
                                              "Rental & Leasing"     => "Real Estate - Property Management",
                                              'default' => 'Financial Services'),
        "Food & Beverage"            => "Food & Beverage",
        "Furniture"                  => array("Retail" => "Retail", 'default' => 'Manufacturing'),
        "Government"                 => array('default' => 'Government'),

        "Hardware"                   => array('Retail Stores' => 'Retail',
                                              'default'       => 'Manufacturing'),
        "Healthcare & Medical"       => array('default' => 'Healthcare/Medical'),
        "Home & Garden"              => array('Landscaping' => 'Business Services',
                                              'Machinery'   => "Manufacturing",
                                              'default'     => 'Retail',),
        "Hospitality & Travel"       => array('default' => 'Travel & Leisure'),
        "Manufacturing"              => array('default' => 'Manufacturing'),
        "Media & Entertainment"      => array('default' => 'Media'),
        "Mining"                     => array('default' => 'Utilities'),
        "Pharmaceuticals"            => array('default' => 'Healthcare/Medical'),
        "Printing & Publishing"      => array('Printed Products' => 'Retail',
                                              'Paper Products'   => "Manufacturing",
                                              'default'          => 'Business Services'),
        "Real Estate"                => array('Agents & Brokerage'  => 'Real Estate - Broker/Owner',
                                              'Property Management' => "Real Estate - Property Management",
                                              'default'             => 'Real Estate - Commercial'),
        "Recreation"                 => array('Boating'                => 'Travel & Leisure',
                                              'Parks & Facilities'     => "Travel & Leisure",
                                              'Photography'            => "Consumer Services",
                                              'Services'               => "Travel & Leisure",
                                              'Firearms'               => "Manufacturing",
                                              'Motorcycles & Bicycles' => "Manufacturing",
                                              'Unclassified'           => "Travel & Leisure",
                                              'default'                => 'Retail'),
        "Retail & Distribution"      => array('default' => 'Retail'),
        "Software & Technology"      => array('default' => 'Technology'),
        "Telecommunications"         => array('default' => 'Telecommunications'),
        "Textiles"                   => array('default' => 'Manufacturing'),
        "Transportation & Logistics" => array('default' => 'Business Services'),
    );
    public $key = null;


    function getDemandBaseData($ip = null) {

        if (!empty($_GET['force_ip']))
            $ip = $_GET['force_ip'];
        if ($ip == null)
            $ip = self::getClientIP();
        if ((isset($_SESSION['demand_base_ip_data'])) && (empty($_GET['force_ip']))) {
            return $_SESSION['demand_base_ip_data'];
        }
        else {
            $url = "https://api.demandbase.com/api/v2/ip.json?key=84e410ecc65c06011997db80b4142a19e8e6f2d7&query=" . $ip;
            if (function_exists('drupal_http_request'))
            {
            $data = drupal_http_request($url);
                $result = json_decode($data->data,true);
            }else{

                $ch = curl_init();

                // set URL and other appropriate options
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch , CURLOPT_SSL_VERIFYPEER, 0 );
                curl_setopt($ch , CURLOPT_SSL_VERIFYHOST, 0 );
                // grab URL and pass it to the browser
                $response = curl_exec($ch);
                $result = json_decode($response,true);
                curl_close($ch);
            }

            $result['ds_industry'] = $this->normalizeIndustry($result['industry'],$result['sub_industry']);


            $_SESSION['demand_base_ip_data'] = $result;

            return $result;
        }

    }
    function is_demandBase($query,$ip = null){

        $data = $this->getDemandBaseData($ip);
        foreach($query as $field => $value)
        {
            if (!is_array($value))
            {   $option = str_replace(' & ','/',$value);
                $value = array($value);
                if ($option != $value[0])
                    $value[] = $option;
            }
            $field = str_replace('-','_',$field);
            if (! in_array($data[$field], $value))
            {
                return false;
            }
        }
        return true;

    }

    static function normalizeIndustry($db_prim_ind, $db_sub_ind = '') {
        $industry = null;
        if (isset(self::$_industryMap[$db_prim_ind]) && is_array(self::$_industryMap[$db_prim_ind])) {
            $industry = isset(self::$_industryMap[$db_prim_ind][$db_sub_ind]) ? self::$_industryMap[$db_prim_ind][$db_sub_ind] : $industry;
            $industry = (!isset($industry) && isset(self::$_industryMap[$db_prim_ind]['default'])) ? self::$_industryMap[$db_prim_ind]['default'] : $industry;
        }
        else
            $industry = (!isset($industry) && isset(self::$_industryMap[$db_prim_ind])) ? self::$_industryMap[$db_prim_ind] : $industry;

        $industry = (!isset($industry)) ? 'Other' : $industry;

        return $industry;
    }
    function getClientIP() {

        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }

    function query($query, $ip = null)
    {
        if (!empty($query)) {
            $selector = new DMongoSelector();
            $data = $this->getDemandBaseData($query, $ip);
            return $selector->compileDocumentSelector($query,$data);
        }
        return true;
    }
}

function is_demandBase($query,$ip = null)
{
    $d_base = new DDemandBase();
    return $d_base->is_demandBase($query,$ip);

}

