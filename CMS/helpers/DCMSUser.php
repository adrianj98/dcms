<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 1/20/14
 * Time: 5:31 PM
 */

namespace CMS\helpers;


class DCMSUser {
    public $username ;
    public $roles;
    public $session = null;
    private $registry;
    function __construct($registry,$session){
        $this->registry = $registry;
        $this->session = $session;
        $this->setUser($session->CMS_USER);
    }
    function setUser($user){

        if ($user){
            $this->username = $user['username'];
            if (is_array($user['roles']))
                $this->roles = $user['roles'];
            else
                $this->roles = explode(",",$user['roles']);
        }
    }
    function isInRole($roles){
        if (is_array($roles['auth']))
            $roles = array_merge($roles['auth'],$roles['view']);
        if (!is_array($roles))
            $roles = explode(",",$roles);
        foreach($roles as $role){
            if (in_array($role,$this->roles))
                 return true;
        }
        return false;
    }
    function canView($roles){
        if (is_array($roles['roles']))
            $roles =  $roles['view']  ;
        return $this->isInRole($roles);

    }
    function canEdit($roles){
        if (is_array($roles['auth']))
            $roles =  $roles['auth'];

        return $this->isInRole($roles);

    }
    function login($user){



        $user['author'] = in_array('admin',$user['roles'] )|| in_array('editor',$user['roles'] );
        $this->session->CMS_USER  = $user;
        return $user;

    }
}