<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 4/21/14
 * Time: 12:37 PM
 */

namespace CMS\helpers\types;



class TDocList extends  TDocTypes
{
    private $index = -1;

    function start()
    {
        $this->index = -1;
        if ($this->cms->editing) {
            return '<span class="insertCms cmsInsertbuttons add" cmsDocId="' . $this->cms->_id . '" property="' . $this->namespace . '.items"   >Insert</span>';
        }
    }

    function ns($namespace = null)
    {
        if ($namespace) {
            $this->namespace = ($namespace);
            return;
        }
        return $this->namespace . '.items.' . $this->index;
    }

    function next()
    {
        $data = $this->getData();
        $this->index++;
        $this->content = $data['items'][$this->index];

        return $this->content;
    }

}

