<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 2/5/14
 * Time: 3:48 PM
 */

namespace CMS\helpers\types;


class TDocDemandBase extends TDocTypes{
    function updateSchema(&$scheme)
    {
        parent::updateSchema($scheme);

    }

    function renderControl()
    {
        return  "
        var factory = {
        value : function(item){
        var result = null;
        var value = $('[property=\"'+item.ns+'\"]').val();
        if (value.trim() !== '')
             result  = jQuery.parseJSON(value);

            return result;
        }
        ,
        create :
        function(item){
            var value =  JSON.stringify(item.value||'');
               var edit =    $('<input value=\"' +value.replace(/\"/g, '&quot;') + '\" />');
            edit.attr('property',item.ns);
            return edit;
        },
           display :
        function(data,field){
        if (data[field.ns])
            return  JSON.stringify(data[field.ns]);
            return '';

        }
        };
        dcms.registerEditFactory('TDocDemandBase',factory);
    ";
    }
} 