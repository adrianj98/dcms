<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 2/5/14
 * Time: 3:49 PM
 */

namespace CMS\helpers\types;


class TDocTypes
{


    protected $content;
    private $_parent = null;
    protected $_data;
    private $_namespace;
    public $cms;
    protected $schema;



    function __construct($cms)
    {
        $this->cms = $cms;
        $this->cms->registerType(get_class($this), $this);

    }

    function renderControl()
    {

    }

    function updateSchema(&$scheme)
    {
        $this->schema = $scheme;
        if ($this->getData())
            $scheme['value'] = $this->getData();
        if ($this->ns())
            $scheme['ns'] = $this->ns();
    }


    //cmsEditbuttons
    function edit($field, $default = null, $attr = null)
    {
        if ($this->cms->editing) {
            $default = isset($default) ? $default : $field;
            $text = $this->getElement($field, $namespace);
            if (!isset($text))
                $text = isset($default) ? $default : $field;
            return '<span class="editableCms cmsEditbuttons edit" cmsAttr="' . $attr . '" cmsDocId="' . $this->cms->_id . '" property="' . $this->ns() . '.' . $namespace . '"  cmsField="' . $field . '" contentValue="' . urlencode($text) . '" ></span>';

        }
    }

    function markdown($field, $default = null)
    {
        return $this->text($field, $default, true);
    }

    function text($field, $default = null, $markdown = false)
    {
        $namespace = null;
        $default = isset($default) ? $default : $field;
        $text = $this->getElement($field, $namespace);
        if (!isset($text))
            $text = isset($default) ? $default : $field;
        if (is_object($text))
            $text = $text->toString();
        $text = \i18::local($text);

        $result = $markdown ? Markdown($text) : $text;
        $namespace = isset($namespace)?$namespace:'content.'.$field;
        //
        if ($this->cms->editing) {
            return '<span class="editableCms useAlt edit" cmsDocId="' . $this->cms->_id . '" property="' . $this->ns() . '.' . $namespace . '"  cmsField="' . $field . '" contentValue="' . urlencode($text) . '">' . $result . '</span>';
        }
        //    $result = Markdown($result);
        return $result;
    }

    function value($field, $default = null)
    {
        $namespace = null;

        $text = $this->getElement($field, $namespace, null);

        $text = isset($text) ? $text : $default;
        if (is_object($text))
            $text = $text->toString();

        return $text;
    }

    function getElement($field, &$namespace = null, $content = 'content')
    {
        if ($content)
            $value = $this->_date[$content][$field];
        else
            $value = $this->_date[$field];

        $namespace = $field;
        return $value;
    }

    function setData($data)
    {
        $this->_data = $data;
    }

    function getData()
    {
        return $this->_data;
    }

    function toString()
    {
        return $this->_data;
    }

    function parent($parent = false)
    {
        if ($parent !== false) {
            $this->_parent = $parent;
        }

        return $this->_parent;
    }

    function ns($namespace = false)
    {
        if ($namespace !== false) {
            $this->_namespace = $namespace;
        }


        return $this->_namespace;
    }

}

class TDocSelect extends TDocTypes
{
    function updateSchema(&$scheme)
    {
        parent::updateSchema($scheme);
        $this->schema = $scheme;
        if (!$scheme['options']){
            $collection = $scheme['collection'];
            $query = $scheme['collection_query'] ? $scheme['collection_query'] : array();
            $options = array();
            $result = $this->cms->registry->mongo->getSchema($collection)->find($query);
            foreach ($result as $key => $item) {
                if ($scheme['collection_text'] == '@index')
                    $value = $key;
                else
                    $value = $item[$scheme['collection_text']];
                if ($scheme['collection_key'] !== '@index')
                    $key = $item[$scheme['collection_key']];

                $options[$key] = $value;

            }
            $scheme['options'] = $options;
            $this->schema = $scheme;
        }
    }

    function toString()
    {
        if (isset($this->schema['collection_value'])) {
            $collection = $this->schema['collection'];
            $query = array('id' => $this->_data);

            $result = $this->cms->registry->mongo->getSchema($collection)->findOne($query);
            return $result[$this->schema['collection_value']];
        } else
            return $this->_data;

    }

    function renderControl()
    {
        return  "
        var factory = {
        value : function(item){
            var value  = $('[property=\"'+item.ns+'\"]').val();

            return value;
        }
        ,
        create :
        function(item){
            var edit = dcms.create_select_ui(item.id, '', item.options, item.value);
            edit.attr('property',item.ns);

            return edit;
        }
        };
        dcms.registerEditFactory('TDocSelect',factory);
    ";
    }
}


class TDocBool extends TDocTypes
{
    function renderControl()
    {
        return  "
        var factory = {
        value : function(item){
            var value  = $('[property=\"'+item.ns+'\"]').is(':checked');

            return value;
        }
        ,
        create :
        function(item){

            var  edit = $('<input type=\"checkbox\"  value=\"1\" ' + (item.value?'checked':'') + '/>');
            edit.attr('property',item.ns);
            return edit;

        }
        }
        dcms.registerEditFactory('TDocBool',factory);
    ";
    }
}


class TDocText extends TDocTypes
{
    function renderControl()
    {
        return  "       dcms.registerEditFactory('TDocText',function(item){
       var value =  item.value||'';
            var edit =    $('<input value=\"' +value + '\" />');
            edit.attr('property',item.ns);
            return edit;
        });
    ";
    }
}


class TDocJson extends TDocTypes{
    function updateSchema(&$scheme)
    {
        parent::updateSchema($scheme);

    }

    function renderControl()
    {
        return  "
        var factory = {
        value : function(item){
        var result = null;
        var value = $('[property=\"'+item.ns+'\"]').val();
        if (value.trim() !== '')
             result  = jQuery.parseJSON(value);

            return result;
        }
        ,
        create :
        function(item){
            var value =  JSON.stringify(item.value||'');
               var edit =    $('<input value=\"' +value.replace(/\"/g, '&quot;') + '\" />');
            edit.attr('property',item.ns);
            return edit;
        },
           display :
        function(data,field){
        if (data[field.ns])
            return  JSON.stringify(data[field.ns]);
            return '';

        }
        };
        dcms.registerEditFactory('TDocJson',factory);
    ";
    }
}