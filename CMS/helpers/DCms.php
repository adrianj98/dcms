<?php
namespace CMS\helpers;



class TDocRoot extends types\TDocTypes
{
    function getElement($field, &$namespace, $content = 'content')
    {

        $field = substr(preg_replace("/[^a-zA-z]/", '_', $field), 0, 20);
        $text = null;

            if ($content) {

                if (!empty($this->_data['content'][$field])) {
                    $namespace = 'content.' . $field;
                    $text = $this->_data['content'][$field];

                }
            } else {
                if (!empty($this->_data[$field])) {
                    $namespace =   $field;
                    $text = $this->_data[$field];

                }
            }



        //  $namespace = $content.'.' . $field;
        return $text;
    }
    function updateSchema(&$schema)
    {




        foreach ($schema['fields'] as $fieldName => &$field) {
            $type = $field['type'];
            $text = null;
            $namespace = null;
            $typeName = "\\CMS\\helpers\\types\\".$type;

            if (!empty($this->_data[$fieldName])) {
                $namespace =   $fieldName;

                if ($type) {
                    $typeObj = new $typeName($this->cms);
                    $typeObj->ns( $namespace);
                    $typeObj->setData($this->_data[$fieldName]);
                    $this->_data[$fieldName] = $typeObj;
                    $typeObj->updateSchema($field);
                }

            }




            if ($type && empty($namespace)) {
                $typeObj = new $typeName($this->cms);
                $typeObj->ns( $fieldName);
                $this->_data[$fieldName] = $typeObj;
                $typeObj->updateSchema($field);
            }

        }


    }

}


class DCms extends \baseClass
{

    private $variants;
    private $schema;

    private $root;
    public $editing = false;


    private $_variant_id;
    private $_contentScheme;
    static $_docTypes = array();

    function getSchema(){
        return $this->schema;
    }
    function __construct($registry, $options = null) {
        parent::__construct($registry, $options );
        $this->root = new TDocRoot($this);
        $this->root->ns('root');
    }

    function checkCollections(){

        //$users =  $this->registry->mongo->getSchema('users')->find();

        $collections = $this->registry->mongo->getSchema('collections')->find();

        foreach($collections as $collection){

            $collectionObj = $this->registry->mongo->getSchema($collection['schema']);
            $collectionObj->install();
        }



    }

    function getId()
    {

        return $this->_id;
    }

    function editOverlay()
    {
        if ($this->editing)
            return $this->registry->template->render('CMS:edit');
    }



 /*   function getSchema()
    {
        return $this->schema;
    }*/

  /*  function realiseScheme()
    {

    }*/



    function setMeta()
    {
        $metas = $this->root->value('meta');
        foreach ($metas as $meta)
            $this->registry->template->addMeta($meta['ref'] ? $meta['ref'] : $meta['name'], $meta);

    }

    function getVariantId()
    {
        return $this->variant_id;
    }


  function updateSchema(&$schema){
      $this->root->updateSchema($schema);

  }


 /*   function compare($field, $value)
    {
        $field = explode('.', $field, 3);

        $field = trim($field[2], '.');
        $stored = $this->text($field, null, true);
        return strcmp($value, $stored);
    }*/

    function updateField($field, $value)
    {
        $field = str_replace('*.', 'variants.default.', $field);
        $field = explode('.', $field, 3);
        $request = array('$set' => array($field[2] => $value));
        $this->registry->mongo->getSchema('variants')->update(array('docId' => $this->getId(), 'variant_id' => $this->getVariantId()), $request);


    }

    function inheritFrom($parent_id, $newName)
    {
        print_r(array($this->getId(), $parent_id, $newName));
        if ($this->getId() && $parent_id && $newName)
            $this->registry->mongo->getSchema('variants')->insert(array('docId' => $this->getId(), 'variant_id' => $newName, 'extends_id' => $parent_id));
        return $this->registry->mongo->getSchema('variants')->findOne(array('docId' => $this->getId(), 'variant_id' => $newName));

    }

    function cloneFrom($parent_id, $newName)
    {
        print_r(array($this->getId(), $parent_id, $newName));
        if ($this->getId() && $parent_id && $newName) {
            $newdoc = $this->registry->mongo->getSchema('variants')->findOne(array('docId' => $this->getId(), 'variant_id' => $parent_id));
            $newdoc['variant_id'] = $newName;
            unset($newdoc['_id']);
            $this->registry->mongo->getSchema('variants')->insert($newdoc);

        }
        return $this->registry->mongo->getSchema('variants')->findOne(array('docId' => $this->getId(), 'variant_id' => $newName));

    }

    function insertItem($field)
    {
        // $field = str_replace('*.', 'default.', $field);
        // $request = array('$push' => array($field => (array('_id' => $this->registry->mongo->id()))));
        // $this->registry->mongo->getCollection('documents')->update(array('docId' => $this->getId()), $request);

    }

    function setup()
    {

        if (!$this->registry->template->cms)
            $this->registry->template->cms = $this;

        if ($this->root->value('lang'))
            \i18::setLanguage($this->root->value('lang'));

        $this->_contentScheme = $this->_id . '!' . $this->variant_id;

        $this->root->updateSchema($this->schema);

        //set templates
        $view = $this->root->value('view');
        if (!is_array($view))
            $view = array('main'=>$view);
        foreach($view as $view_id => $viewspace){
            $this->registry->template->setView($view_id, $viewspace);
        }


        $this->setMeta();


    }

    function getVariantIds()
    {
        $schema = $this->registry->mongo->getSchema($this->_id);
        $variants =  $schema ->find(array());
        foreach ($variants as $key => $value) {
            $result[] = $value['variant_id'];
        }
        return $result;
    }

    function getDoc($schemaName,$variant_id)
    {

    //    $this->docType = $this->registry->mongo->getSchema('documents')->findOne(array('docId' => $docId));


    //    $mapping = $this->docType['mapping'][$variant_id];

    //    $variant_id = isset($mapping) ? $mapping : $variant_id;
        $this->variant_id = $variant_id;

        $schema = $this->registry->mongo->getSchema($schemaName);
        $this->schema = $schema->getSchema();
        $variant =  $schema ->findOne(array( 'variant_id' => $variant_id),array(),array('mergeInherited'=>true));

        $this->root->setData($variant);



    }

    function load($namespace, $variant = null)
    {
        if ($_GET['forceVariant'])
            $variant = $_GET['forceVariant'];
        \DCore::using('CMS:helpers/DCmsTwig');
        $this->_id = $namespace;

        $this->getDoc($namespace, $variant);

        $this->setup();
    }

    function text($field, $default = null)
    {
        $result = $this->root->text($field, $default);

        return $result;
    }

    function edit($field, $default = null)
    {
        $result = $this->root->edit($field, $default);

        return $result;
    }

    function with($field)
    {
        $namespace = null;

        $element = $this->currentElement->getElement($field, $namespace);
        if ($this->currentElement)
            $element->parent($this->currentElement);
        $this->currentElement = $element;

        return $element;

    }



    function value($field, $default = null)
    {

        $result = $this->root->value($field, $default);

        return $result;
    }

    function startTag()
    {
        if ($this->editing)
            return '<div about="' . $this->_id . '^' . $this->variant_id . '" >';
        return '';
    }

    function endTag()
    {
        if ($this->editing)
            return '</div>';
        return '';
    }

    function renderJS()
    {
        $result = '';
        foreach (self::$_docTypes as $type) {
            $result .= $type->renderControl();


        }
        return $result;
    }

    static function registerType($docTypeName, $docType)
    {
        self::$_docTypes[$docTypeName] = $docType;
    }

} 