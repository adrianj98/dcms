<?php
/**
 * Created by PhpStorm.
 * User: adrian.jones
 * Date: 1/6/14
 * Time: 3:17 PM
 */


class DCms_text_TokenParser extends Twig_TokenParser
{
    public function parse(Twig_Token $token)
    {
        $parser = $this->parser;
        $stream = $parser->getStream();

        if ( !$stream->test(Twig_Token::BLOCK_END_TYPE,null))
            $name = $parser->getExpressionParser()->parseExpression();
        $stream->expect(Twig_Token::BLOCK_END_TYPE);
        $body = $parser->subparse(array($this, 'endtext'));
        $this->parser->getStream()->next()->getValue();
        $this->parser->getStream()->expect(Twig_Token::BLOCK_END_TYPE);
        return new DCms_text_Node($name,$body,  $token->getLine(), $this->getTag());
    }
    public function endtext(Twig_Token $token)
    {
        return $token->test(array('endtext'));
    }
    public function getTag()
    {
        return 'text';
    }
}

class DCms_text_Node extends Twig_Node
{
    public function __construct($name,$body,  $line, $tag = null)
    {
        parent::__construct( array('name' => $name,"body"=>$body), array( ),$line, $tag);
    }

    public function compile(Twig_Compiler $compiler)
    {

        $compiler
            ->write('ob_start();')
            ->subcompile($this->getNode('body'))
        ->write('$body = ob_get_clean ();');

        $statment = $compiler
            ->addDebugInfo($this)
            ->write('echo $context["cms"]->text(')
            ->subcompile($this->getNode('name'));
        $statment
            ->write(',')
            ->raw('$body');

        $statment
           ->raw(");\n");


    }
}

class DCms_Twig_Extension extends Twig_Extension
{
    public function getName()
    {
        return 'DCms';
    }
    public function getGlobals(){
        global $registry;
        if ($registry->template->cms)
            return array(
                'cms' => $registry->template->cms,
            );

          return array();
    }
    public function getTokenParsers()
    {
        return array(new DCms_text_TokenParser());
    }


}

DTwig::$extensions["DCms_Twig_Extension"] = "DCms_Twig_Extension";