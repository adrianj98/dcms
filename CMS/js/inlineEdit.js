var view_list = ['default', 'Blue Benifit', 'Notary']
var prov_list = ['', "nar_trial",
    "freetrial",
    "businesstrial",
    "protrial",
    "reprotrial" ,
    "devaccount", "ink" , "personal" , "narfree" , "adminsrock" , "nflpa" , "protrial_30day", "intel" , "default"];
var custom_toolbar_class = 'create-ui-tool-customarea';


(function($){



    var logout_callback = function () {
        window.location.href = '/code/cms/login';
    }

    var variant_callback = function () {
        var variant = $(this).val();
        if (variant == 'default')
            window.location.href = '/trial';
        else
            window.location.href = '/trial?forceVariant=' + variant;
    }
    inherit_callback = function () {
        $("#variant-select-form").dialog({
            autoOpen: false,

            modal: true,
            buttons: {
                "Create an variant": function () {
                    var variantname = $('#variant-name').val();
                    $.ajax({
                        type: "POST",
                        url: '/code/api/cms/inherit',
                        data: {document: doc_id, parent_id: variant_id, newName: variantname},
                        success: function (resp) {
                            // options.success(resp);
                            window.location.href = '/trial?forceVariant=' + variantname;

                        }

                    })
                }
            }
        });
        startEditMode();
        $("#variant-select-form").dialog("open");

    }
    clone_callback = function () {
        $("#variant-select-form").dialog({
            autoOpen: false,

            modal: true,
            buttons: {
                "Create an variant": function () {
                    var variantname = $('#variant-name').val();
                    $.ajax({
                        type: "POST",
                        url: '/code/api/cms/clone',
                        data: {document: doc_id, parent_id: variant_id, newName: variantname},
                        success: function (resp) {
                            // options.success(resp);
                            window.location.href = '/trial?forceVariant=' + variantname;

                        }

                    })
                }
            }
        });

        $("#variant-select-form").dialog("open");

    }

    config_callback = function () {
        $('#editwindowConfigure').toggle();
    }
// add logout button, no matter what






    Backbone.sync = function (method, model, options) {

        var changed = {};
        _.each(model._originalAttributes, function (item, key) {
            if (item !== model.attributes[key])
                changed[key] = model.attributes[key];

        });
        console.log(changed);
        var valueControls  = $('#editwindowConfigure input, #editwindowConfigure select');
        var values = {},value;
        valueControls.each(function(index){
            var control =  $(valueControls[index]);
            if(control.attr('type') == 'checkbox')
                value = control.prop('checked');
            else
                value = control.val();
            var property = control.attr('property');
            values[property] = value;

        });
        var postValues = {};
        _.each(configItems.fields,function(item){
            if ((item.value != values[item.ns]) && !((item.value === undefined) && (values[item.ns] == "")))
                changed["<http://viejs.org/ns/" +item.ns] = values[item.ns];
        });

        changed['@subject'] = model['@subject'];
        $.ajax({
            type: "POST",
            url: '/code/api/cms/update',
            data: {'content' : changed, values:postValues},
            success: function (resp) {
                options.success(resp);


            }
//         dataType: 'json'
        });


    }




    /* adds a custom ui button to the midgard create toolbar */
    dcms.add_custom_ui_button = function(id, label, enabled_callback, disabled_callback) {
        // create the custom button only if it is not already existing
        if ($("button#" + id).length > 0) {
            return;
        }

        var custom_button = $(
            "<button role='button' class='create-ui-btn ui-button ui-widget ui-corner-all ui-state-default ui-button-text-only' id='" + id + "'>" +
                "<span class='ui-button-text'>" + label + "</span>" +
                "</button>"
        );

        custom_button.hover(function () {
                $(this).addClass("ui-state-hover");
            },
            function () {
                $(this).removeClass("ui-state-hover");
            });

        custom_button.bind("click", function (event) {
            var is_active = $(this).hasClass("ui-state-active");
            if (!is_active) {
                enabled_callback();
                $(this).addClass("ui-state-active");
            }
            else {
                disabled_callback();
                $(this).removeClass("ui-state-active");
            }
        });

// add the button, wrapped by custom_buttons div
        var wrapper = $("div.create-ui-toolbar-dynamictoolarea li." + custom_toolbar_class);
        if (wrapper.length == 0) {
            // insert after workflows holder
            wrapper = $("<li class='" + custom_toolbar_class + "'></li>").insertAfter(
                $("div.create-ui-toolbar-dynamictoolarea li.create-ui-tool-workflowarea")
            );
        }

        custom_button.appendTo(wrapper);
    }
    dcms.initCreateJs = function(options){
        jQuery('body').midgardCreate({
            url: function () {
                return 'http://local.drupal/code/api/cms/update';
            }
        });



       // dcms.createPanel( $('.create-ui-toolbar-wrapper'),configItems.fields);
        dcms.add_custom_ui_button('midgard-logout', 'Logout', logout_callback, function () {
        });
        dcms.add_custom_ui_select('midgard-select', 'Variants', variant_list, variant_id, variant_callback, function () {
        });
    //    dcms.add_custom_ui_button('midgard-inherit', 'Inherit', inherit_callback, function () {
    //    });
    //    dcms.add_custom_ui_button('midgard-clone', 'Clone', clone_callback, function () {
     //   });
     //   dcms.add_custom_ui_button('midgard-config', 'Configure', config_callback, function () {
     //   });
    }

})(jQuery);